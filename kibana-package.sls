kibana-repo:
  pkgrepo.managed:
    - humanname: Kibana repository for 5.x packages
    - name: kibana-5.x
    - baseurl: https://artifacts.elastic.co/packages/5.x/yum
    - gpgcheck: 1
    - gpgkey: https://artifacts.elastic.co/GPG-KEY-elasticsearch
    - enabled: 1

kibana:
  pkg:
    - installed
    - require:
      - pkgrepo: kibana-repo
      - pkg: java-1.8.0-openjdk
