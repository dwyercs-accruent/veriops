/etc/httpd/conf.d/kiproxy.conf:
  file.managed:
    - source: salt://httpd/kiproxy.conf
  require:
    - pkg:
      - httpd
      - kibana
