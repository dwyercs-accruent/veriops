/etc/elasticsearch/elasticsearch.yml:
  file.managed:
    - contents: 
      - "cluster.name: example-cluster"
      - "node.name: example-cluster-00"
      
      - "path.data: /opt/elasticsearch"
      - "path.logs: /var/log/elasticsearch"

      - "index.store.type: mmapfs"

      - "network.host: 1.2.3.4 <---change to this host's accessible IP address"
      - "http.port: 9200"

      - "discovery.zen.ping.unicast.hosts: ['1.2.3.4:9300']<---change the single quotes to doubles"
      - "discovery.zen.minimum_master_nodes: 2"

