/etc/elasticsearch/curator-config.yml:
  file.managed:
    - source: salt://curator/curator-config.yml
/etc/elasticsearch/curator-action.yml:
  file.managed:
    - source: salt://curator/curator-action.yml
