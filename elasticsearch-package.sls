elasticsearch-repo:
  pkgrepo.managed:
    - humanname: Elasticsearch repository for 5.x packages
    - name: elasticsearch-5.x
    - baseurl: https://artifacts.elastic.co/packages/5.x/yum
    - gpgcheck: 1
    - gpgkey: https://artifacts.elastic.co/GPG-KEY-elasticsearch
    - enabled: 1

curator-repo:
  pkgrepo.managed:
    - humanname: CentOS/RHEL 6 repository for Elasticsearch Curator 4.x packages
    - name: curator-4
    - baseurl: http://packages.elastic.co/curator/4/centos/6
    - gpgcheck: 1
    - gpgkey: http://packages.elastic.co/GPG-KEY-elasticsearch
    - enabled: 1

elasticsearch:
  pkg:
    - installed
    - require:
      - pkgrepo: elasticsearch-repo
      - pkg: java-1.8.0-openjdk

elasticsearch-curator:
  pkg:
    - installed
    - require:
      - pkgrepo: curator-repo
      - pkg: java-1.8.0-openjdk
      - pkg: elasticsearch
