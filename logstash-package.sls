logstash-repo:
  pkgrepo.managed:
    - humanname: Logstash repository for 5.x packages
    - name: logstash-5.x
    - baseurl: https://artifacts.elastic.co/packages/5.x/yum
    - gpgcheck: 1
    - gpgkey: https://artifacts.elastic.co/GPG-KEY-elasticsearch
    - enabled: 1

logstash:
  pkg:
    - installed
    - require:
      - pkgrepo: logstash-repo
      - pkg: java-1.8.0-openjdk
