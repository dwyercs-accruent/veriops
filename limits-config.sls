/etc/security/limits.d/90-nproc.conf:
  file.append:
    - text: 
      - elasticsearch   soft    nproc           4096
      - elasticsearch   hard    nproc           4096
      - elasticsearch   soft    nofile          65536
      - elasticsearch   hard    nofile          65536
