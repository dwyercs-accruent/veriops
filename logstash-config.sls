/etc/logstash/logstash.yml:
  file.managed:
    - contents: 
      - "node.name: hex-log-00"
      - "path.data: /var/lib/logstash"
      - "path.config: /etc/logstash/conf.d"
      - "http.host: '1.2.3.4' <--- change to doblequotes"
      - "path.logs: /var/log/logstash"
    - require:
      - pkg: logstash
