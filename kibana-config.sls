/etc/kibana/kibana.yml:
  file.managed:
    - contents: 
      - "server.host: '127.0.0.1' <---v--- change all quotes to doubles"
      - "server.port: 5601"
      - "server.name: 'example-cluster-00'"
      - "elasticsearch.url: 'http://1.2.3.4:9200'"
    - require:
      - pkg: kibana
